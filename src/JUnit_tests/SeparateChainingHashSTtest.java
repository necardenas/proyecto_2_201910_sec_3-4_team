package JUnit_tests;

import org.junit.*; 
import model.data_structures.*; 

import static org.junit.Assert.*;

import java.io.IOException; 
import java.security.NoSuchAlgorithmException; 

import org.junit.Test; 

/**
 * Hash table unit tests 
 *  
 * @author markus.korbel 
 *  
 */ 
public class SeparateChainingHashSTtest { 

	@Test 
	public void basicStorageTest() throws IllegalArgumentException, 
	NoSuchAlgorithmException, IOException { 
		SeparateChainingHashST<Integer, String> SeparateChainingHashST = new SeparateChainingHashST<Integer, String>(1); 

		// Add item and check 
		SeparateChainingHashST.put(4469, "markus"); 
		assertEquals(1, SeparateChainingHashST.size()); 
		assertTrue(SeparateChainingHashST.contains(4469)); 
		assertEquals("markus", SeparateChainingHashST.get(4469)); 

		// Add item and check 
		SeparateChainingHashST.put(1234, "test"); 
		assertEquals(2, SeparateChainingHashST.size()); 
		assertTrue(SeparateChainingHashST.contains(1234)); 
		assertEquals("test", SeparateChainingHashST.get(1234)); 

		// Check get with non-existent 
		assertNull(SeparateChainingHashST.get(0)); 

		// New remove an item 
		SeparateChainingHashST.delete(1234); 
		assertFalse(SeparateChainingHashST.contains(1234)); 

		// Remove non-existent 
		SeparateChainingHashST.delete(0); 
	} 

	/**
	 * Tests if all elements are stored and retrieved correctly even with 
	 * collisions 
	 *  
	 * @throws IllegalArgumentException 
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 */ 
	@Test 
	public void collisionTest() throws IllegalArgumentException, 
	NoSuchAlgorithmException, IOException { 
		// Hash table has 256 slots, if we add more we are guaranteed to have 
		// collisions 
		SeparateChainingHashST<Integer, String> SeparateChainingHashST = new SeparateChainingHashST<Integer, String>(1); 

		// Add 1000 elements with random (no duplicates!) strings 
		String[] randomStrings = RandomArray.randomStringArray(1000, 10, false, 
				false); 
		for (int i = 0; i < 1000; i++) { 
			SeparateChainingHashST.put(i, randomStrings[i]); 
		} 

		assertEquals(1000, SeparateChainingHashST.size()); 

		// Check if all keys are found 
		for (int i = 0; i < 1000; i++) { 
			assertTrue(SeparateChainingHashST.contains(i)); 
		} 

		// Check if all keys are still mapped to correct random string 
		for (int i = 0; i < 1000; i++) { 
			assertEquals(randomStrings[i], SeparateChainingHashST.get(i)); 
		} 

		// Remove them one by one to see if the hash table scales back correctly 
		for (int i = 0; i < 1000; i++) { 
			SeparateChainingHashST.delete(i); 
			assertFalse(SeparateChainingHashST.contains(i)); 
		} 

		assertEquals(0, SeparateChainingHashST.size()); 
	} 

	/**
	 * Duplicate key exception test 
	 *  
	 * @throws IllegalArgumentException 
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 */ 
	@Test(expected = IllegalArgumentException.class) 
	public void duplicateKeyTest() throws IllegalArgumentException, 
	NoSuchAlgorithmException, IOException { 
		SeparateChainingHashST<Integer, String> SeparateChainingHashST = new SeparateChainingHashST<Integer, String>(1); 
		SeparateChainingHashST.put(4469, "markus"); 
		assertEquals(1, SeparateChainingHashST.size()); 
		assertTrue(SeparateChainingHashST.contains(4469)); 
		SeparateChainingHashST.put(4469, "doesn't matter"); 
	} 

	/**
	 * Illegal MD5 byte count exception tests 
	 *  
	 * @throws NoSuchAlgorithmException 
	 */ 
	@Test 
	public void illegalMD5ByteCountTest() throws NoSuchAlgorithmException { 
		try { 
			new SeparateChainingHashST<Integer, String>(0); 
			fail("Should not reach!"); 
		} catch (IllegalArgumentException e) { 
		} 
		try { 
			new SeparateChainingHashST<Integer, String>(4); 
			fail("Should not reach!"); 
		} catch (IllegalArgumentException e) { 
		} 
	} 
}