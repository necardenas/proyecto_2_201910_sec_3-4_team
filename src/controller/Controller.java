package controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import model.data_structures.ArregloDinamico;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHashST;
import model.data_structures.Stack;
import model.vo.B1;
import model.vo.B2;
import model.vo.B3;
import model.vo.C1;
import model.vo.C4;
import model.vo.ComparadorPorFecha;
import model.vo.ComparadorPorHora;
import model.vo.ComparadorPorLocalizacion;
import model.vo.ComparadorPorSoloHora;
import model.vo.Fecha;
import model.vo.FranjaInfracciones;
import model.vo.Hora;
import model.vo.InformacionLocalizacion;
import model.vo.LocalizacionGeografica;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
import model.util.*;

public class Controller 
{
	public final static String[] mths1 = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio"};

	public final static String[] mths2 = {"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

	public final static String[] mthsNum1 = {"01", "02", "03", "04", "05", "06"};

	public final static String[] mthsNum2 = {"07", "08", "09", "10", "11", "12"};

	private String[] mths;

	private String[] mthsNum;

	private ArrayList<String> mthList;

	private ArrayList<String> mthNumList;

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Queue<VOMovingViolations> queue;

	private VOMovingViolations[] array;

	private RedBlackBST<Hora, Queue<VOMovingViolations>> tree;

	private MaxPQ<InformacionLocalizacion> heap;

	/**
	 * Estructuras de los requerimientos (Parte 2)
	 */
	private ArregloDinamico<VOMovingViolations> list;

	private MaxPQ<B1> b1;

	private RedBlackBST<String, B2> b2;

	private RedBlackBST<Double, B3>b3;

	private C1[] c1;

	private MaxPQ<C4> c4;

	private int minAddress= Integer.MAX_VALUE;

	private int maxAddress = 0;

	private IQueue<B1> req1B;

	private B2 req2B;

	private IQueue<B3> reqB3;

	private C1 req1C;

	private IQueue<C4> req4C;

	/**
	 * Tiempos de ejecuci�n
	 */
	long t1, t2;

	/**
	 * Crea una instancia de la clase
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		mths = new String[6];
		mthsNum = new String[6];
		mthList = new ArrayList<>();
		mthNumList = new ArrayList<>();
		//inicializar las estructuras de datos
		queue = new Queue<>();
		tree = new RedBlackBST<>();
		heap = new MaxPQ<>();
	}

	// M�todo run
	public void run() 
	{
		long startTime;
		long endTime;
		long duration;

		Scanner sc = new Scanner(System.in);
		boolean loaded = false;
		boolean fin = false;

		while (!fin) 
		{
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 1: // CARGAR
				if (!loaded) 
				{
					//Cargar informaci�n
					System.out.println();
					System.out.println("Introduzca un semestre (1-2)");
					System.out.println();
					int sem = sc.nextInt();

					if (sem > 2 || sem < 1)
					{
						System.out.println();
						System.out.println("S�lo se admiten los semestres 1 y 2");
						System.out.println();
						break;
					}

					long start = System.currentTimeMillis();
					this.loadMovingViolations(sem);
					long end = System.currentTimeMillis();
					System.out.println("Tiempo de carga (milis): "+ (end-start));
					System.out.println();

					for (String current: mths)
						mthList.add(current);

					for (String current: mthsNum)
						mthNumList.add(current);

					//Pasar a arreglo
					Queue<VOMovingViolations> copy = queue;
					array = new VOMovingViolations[copy.size()];
					list = new ArregloDinamico<VOMovingViolations>(queue.size());
					int index = 0;
					while (!copy.isEmpty())
					{
						VOMovingViolations element = copy.dequeue();
						array[index] = element;
						list.agregar(element);
						index++;
					}
					loadStructures();
					//Cargar estructura 2C (arbol)

					//Ordenar por hora
					ComparadorPorHora comparator2c = new ComparadorPorHora();
					Sort.quickSort3(array, 0, array.length-1, comparator2c);
					//Insertar en el arbol
					Queue<VOMovingViolations> tempQueue = new Queue<VOMovingViolations>();
					tempQueue.enqueue(array[0]);
					int i = 1;
					while (i<array.length)
					{
						if (comparator2c.compare(array[i-1], array[i]) == 0)
							tempQueue.enqueue(array[i]);
						else
						{
							Hora f = new Hora(array[i-1].getHourObject());
							tree.put(f, tempQueue);
							tempQueue = new Queue<VOMovingViolations>();
							tempQueue.enqueue(array[i]);
						}
						i++;
					}
					Hora f = new Hora(array[i-1].getHourObject());
					tree.put(f, tempQueue);

					//Cargar estructura 3C (heap)

					//Ordenar por localizacion
					ComparadorPorLocalizacion comparator3c = new ComparadorPorLocalizacion();
					Sort.quickSort3(array, 0, array.length-1, comparator3c);
					//Insertar en el heap
					tempQueue = new Queue<>();
					tempQueue.enqueue(array[0]);
					i = 1;
					while (i<array.length)
					{
						if (comparator3c.compare(array[i-1], array[i]) == 0)
							tempQueue.enqueue(array[i]);
						else
						{
							InformacionLocalizacion il = new InformacionLocalizacion(array[i-1].getXCord(), array[i-1].getYCord(), tempQueue);
							heap.insert(il);
							tempQueue = new Queue<>();
							tempQueue.enqueue(array[i]);
						}
						i++;
					}
					InformacionLocalizacion il = new InformacionLocalizacion(array[i-1].getXCord(), array[i-1].getYCord(), tempQueue);
					heap.insert(il);

					//Fin
					loaded = true;
					break;
				} 
				else 
				{
					System.out.println();
					System.out.println("Los datos ya fueron cargados (S�lo se pueden cargar una vez)");
					System.out.println();
					break;
				}

			case 2: //1A
				if (loaded)
				{
					System.out.println("Introduzca el top que quiere obetener");
					int n = sc.nextInt();
					if (n < 1 || n > 24)
					{
						System.out.println("El top debe estar entre 1 y 24");
						break;
					}
					darRankingFranjas(n);
					break;
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
					break;
				}

			case 3: //2A
				if (loaded)
				{
					System.out.println("Introduzca la coordenada en x");
					System.out.println("---Ejemplo: 398728.3");
					String xi = sc.next();
					double x = 0;
					try
					{
						x = Double.parseDouble(xi);
					}
					catch (NumberFormatException e)
					{
						System.out.println("Entrada inv�lida");
						break;
					}
					System.out.println("Introduzca la coordenada en y");
					System.out.println("---Ejemplo: 139835.9");
					String yi = sc.next();
					double y = 0;
					try
					{
						y = Double.parseDouble(yi);
					}
					catch (NumberFormatException e)
					{
						System.out.println("Entrada inv�lida");
						break;
					}
					ordenarPorLocalizacion(x, y);
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
				}
				break;

			case 4: //3A
				if (loaded)
				{
					//Fecha inicial
					System.out.println("Introduzca la fecha inicial");
					System.out.println("---Ejemplo: 2018/03/01");
					String input1 = sc.next();
					if (!input1.contains("/"))
					{
						System.out.println("Entrada inv�lida: La fecha debe estar con el formato yyyy/mm/dd");
						break;
					}
					String[] dArray = input1.split("/");
					if (!dArray[0].equals("2018"))
					{
						System.out.println("Entrada inv�lida: El a�o debe ser 2018");
						break;
					}
					if (!mthNumList.contains(dArray[1]))
					{
						System.out.println("El mes indicado no est� en el semestre");
						break;
					}
					int a�o = Integer.parseInt(dArray[0]);
					int mes = 0, dia = 0;
					try
					{
						mes = Integer.parseInt(dArray[1]);
						dia = Integer.parseInt(dArray[2]);
					}
					catch (Exception e)
					{
						System.out.println("No se pudo convertir la entrada a una fecha espec�fica");
						break;
					}
					if (dia > 31 || dia < 1)
					{
						System.out.println("El d�a introducido es inv�lido");
						break;
					}
					//Fecha final
					System.out.println("Introduzca la fecha final");
					System.out.println("---Ejemplo: 2018/03/01");
					String input2 = sc.next();
					if (!input2.contains("/"))
					{
						System.out.println("Entrada inv�lida: La fecha debe estar con el formato yyyy/mm/dd");
						break;
					}
					String[] dfArray = input2.split("/");
					if (!dfArray[0].equals("2018"))
					{
						System.out.println("Entrada inv�lida: El a�o debe ser 2018");
						break;
					}
					if (!mthNumList.contains(dfArray[1]))
					{
						System.out.println("El mes indicado no est� en el semestre");
						break;
					}
					int a�o2 = Integer.parseInt(dfArray[0]);
					int mes2 = 0, dia2 = 0;
					try
					{
						mes2 = Integer.parseInt(dfArray[1]);
						dia2 = Integer.parseInt(dfArray[2]);
					}
					catch (Exception e)
					{
						System.out.println("No se pudo convertir la entrada a una fecha espec�fica");
						break;
					}
					if (dia2 > 31 || dia2 < 1)
					{
						System.out.println("El d�a introducido es inv�lido");
						break;
					}
					//Convertir y comparar las fechas
					Fecha d1 = new Fecha(a�o, mes, dia);
					Fecha d2 = new Fecha(a�o2, mes2, dia2);
					if (d1.compareTo(d2) > 0)
					{
						System.out.println("La fecha inicial no puede ser mayor a la final");
						break;
					}
					//Dar requerimiento
					darInfraccionesPorFechas(d1, d2);
					break;
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
					break;
				}

			case 5: //1B
				view.printMessage("1B. Consultar los N Tipos con mas infracciones. Ingrese el valor de N: ");
				int numeroTipos = sc.nextInt();

				startTime = System.currentTimeMillis();

				darRankingVioCode(numeroTipos);
				endTime = System.currentTimeMillis();

				duration = endTime - startTime;
				view.printMessage("Tiempo requerimiento 4C: " + duration + " milisegundos");
				//TODO Completar para la invocación del metodo 1B				
				//model.rankingNViolationCodes(int N)

				//TODO Mostrar resultado de tipo Cola con N InfraccionesViolationCode
				view.printReq1B( req1B );
				break;

			case 6: //2B
				view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 1234,56): ");
				double xcoord = sc.nextDouble();
				view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 5678,23): ");
				double ycoord = sc.nextDouble();

				//TODO Completar para la invocación del metodo 2B
				ordenarPorLocalizacionB( xcoord,  ycoord);

				//TODO Mostrar resultado de tipo InfraccionesLocalizacion 
				view.printReq2B( req2B );
				break;

			case 7: //3B
				view.printMessage("Ingrese la cantidad minima de dinero que deben acumular las infracciones en sus rangos de fecha  (Ej. 1234,56)");
				double cantidadMinima = sc.nextDouble();

				view.printMessage("Ingrese la cantidad maxima de dinero que deben acumular las infracciones en sus rangos de fecha (Ej. 5678,23)");
				double cantidadMaxima = sc.nextDouble();

				//TODO Completar para la invocación del metodo 3B
				//model.consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)

				darAcumuladoInfracciones(cantidadMinima, cantidadMaxima);
				//TODO Mostrar resultado de tipo Cola con InfraccionesFechaHora 
				view.printReq3B( reqB3 );
				break;

			case 8: //1C
				view.printMessage("1C. Consultar las infracciones con Address_Id. Ingresar el valor de Address_Id: ");

				int addressID = sc.nextInt();

				/*
				startTime = System.currentTimeMillis();
				darInformacionPorAddressID(addressID);
				endTime = System.currentTimeMillis();
				 **/ 

				double startTimeSeparte = System.nanoTime();
				darInformacionPorAddressID(addressID);
				double endTimeSeparte = System.nanoTime();

				double timeSeparate = (endTimeSeparte - startTimeSeparte);
				System.out.println("tiempo duracion metodo c1 (nanosegundos):"+timeSeparate);

				//TODO Mostrar resultado de tipo InfraccionesLocalizacion 	
				view.printReq1C( req1C );
				break;

			case 9: //2C
				if (loaded)
				{
					//Hora inicial
					System.out.println("Introduzca la hora inicial");
					System.out.println("---Ejemplo: 12:30:00");
					String input1 = sc.next();
					if (!input1.contains(":"))
					{
						System.out.println("Entrada inv�lida: La fecha debe estar con el formato hh:mm:ss");
						break;
					}
					String[] array1 = input1.split(":");
					int hora = 0, minuto = 0, segundo = 0;
					try
					{
						hora = Integer.parseInt(array1[0]);
						minuto = Integer.parseInt(array1[1]);
						segundo = Integer.parseInt(array1[2]);
					}
					catch (NumberFormatException e)
					{
						System.out.println("La entrada es inv�lida");
						break;
					}
					if (hora > 23 || hora < 0)
					{
						System.out.println("La hora inicial es inv�lida");
						break;
					}
					if (minuto > 59 || minuto < 0)
					{
						System.out.println("El minuto inicial es inv�lido");
						break;
					}
					if (segundo > 59 || segundo < 0)
					{
						System.out.println("El segundo inicial es inv�lido");
						break;
					}
					//Hora final
					System.out.println("Introduzca la hora final");
					System.out.println("---Ejemplo: 12:30:00");
					String input2 = sc.next();
					if (!input2.contains(":"))
					{
						System.out.println("Entrada inv�lida: La fecha debe estar con el formato hh:mm:ss");
						break;
					}
					String[] array2 = input2.split(":");
					int hora2 = 0, minuto2 = 0, segundo2 = 0;
					try
					{
						hora2 = Integer.parseInt(array2[0]);
						minuto2 = Integer.parseInt(array2[1]);
						segundo2 = Integer.parseInt(array2[2]);
					}
					catch (NumberFormatException e)
					{
						System.out.println("La entrada es inv�lida");
						break;
					}
					if (hora2 > 23 || hora2 < 0)
					{
						System.out.println("La hora final es inv�lida");
						break;
					}
					if (minuto2 > 59 || minuto2 < 0)
					{
						System.out.println("El minuto final es inv�lido");
						break;
					}
					if (segundo2 > 59 || segundo2 < 0)
					{
						System.out.println("El segundo final es inv�lido");
						break;
					}
					//Validar que h1 < h2
					Hora h1 = new Hora(hora, minuto, segundo);
					Hora h2 = new Hora(hora2, minuto2, segundo2);
					if (h1.compareTo(h2) > 0)
					{
						System.out.println("La hora inicial no puede ser mayor que la final");
						break;
					}
					//Dar requerimiento
					darInfraccionesRangoHoras(h1, h2);
					System.out.println("Tiempo en nanos: " + (t2-t1));
					break;
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
					break;
				}

			case 10: //3C
				if (loaded)
				{
					System.out.println("Introduzca el top que quiere obetener");
					int n = sc.nextInt();
					if (n < 1)
					{
						System.out.println("El top debe ser mayor que 1");
						break;
					}
					darRankingLocalizaciones(n);
					System.out.println("Tiempo en nanos: "+(t2-t1));
					System.out.println();
					break;
				}
				else
				{
					System.out.println("Los datos deben ser cargados primero");
					break;
				}

			case 11: //4C
				System.out.println("Grafica ASCII con la informacion de las infracciones por ViolationCode");

				startTime = System.currentTimeMillis();
				view.printReq4C(c4);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo requerimiento 4C: " + duration + " milisegundos");
				break;

			case 0:
				fin = true;
				sc.close();
				break;
			}
		}
	}

	//LOAD
	/**
	 * Carga los datos de los archivos csv
	 * pre: Las estructuras de datos fueron inicializadas
	 * post: Se cargaron los datos en las estructuras de datos
	 * @param semestre >= 1 && semestre <= 2
	 */
	public void loadMovingViolations(int semestre)
	{
		CsvParserSettings settings = new CsvParserSettings();
		settings.getFormat().setLineSeparator("\n");
		CsvParser parser = new CsvParser(settings);

		String path = ".\\data\\Moving_Violations_Issued_in_";
		String mth1Path = "";
		String mth2Path = "";
		String mth3Path = "";
		String mth4Path = "";
		String mth5Path = "";
		String mth6Path = "";
		mths = new String[6];

		if (semestre == 1)
		{
			mth1Path = path + "January_2018.csv";
			mth2Path = path + "February_2018.csv";
			mth3Path = path + "March_2018.csv";
			mth4Path = path + "April_2018.csv";
			mth5Path = path + "May_2018.csv";
			mth6Path = path + "June_2018.csv";
			mths = mths1;
			mthsNum = mthsNum1;
		}

		else if (semestre == 2)
		{
			mth1Path = path + "July_2018.csv";
			mth2Path = path + "August_2018.csv";
			mth3Path = path + "September_2018.csv";
			mth4Path = path + "October_2018.csv";
			mth5Path = path + "November_2018.csv";
			mth6Path = path + "December_2018.csv";
			mths = mths2;
			mthsNum = mthsNum2;
		}

		String[] paths = {mth1Path, mth2Path, mth3Path, mth4Path, mth5Path, mth6Path};
		String cPath = "";

		int numInfracs = 0;
		double xMin = Double.MAX_VALUE, yMin = Double.MAX_VALUE, xMax = 0, yMax = 0;
		try
		{
			int i = 0;
			while (i < paths.length)
			{
				int tempInfrac = 0;
				cPath = paths[i];
				parser.beginParsing(new FileReader(cPath));
				parser.parseNext();
				String[] row = null;
				int a = 0, b = 0, c = 0;
				if (i > 2 && semestre == 2)
				{
					a = 14; b = 15; c = 16;
				}
				else
				{
					a = 13; b = 14; c = 15;
				}
				while((row = parser.parseNext()) != null)
				{
					String in1 = row[0].toString();
					Integer objId = Integer.parseInt(in1);

					String location = row[2].toString();

					int addressId = 0;
					if (row[3] != null)
					{
						String in2 = row[3].toString();
						addressId = Integer.parseInt(in2);
					}

					double streetId = 0;
					if (row[4] != null)
					{
						String in3 = row[4].toString();
						streetId = Double.parseDouble(in3);
					}

					String in4 = row[5].toString();
					Double xCord = Double.parseDouble(in4);

					String in5 = row[6].toString();
					Double yCord = Double.parseDouble(in5);

					String ticketType = row[7].toString();

					String in6 = row[8].toString();
					Integer fineAmt = Integer.parseInt(in6);

					String in7 = row[9].toString();
					double totalPaid = Double.parseDouble(in7);

					String in8 = row[10].toString();
					Integer penalty1 = 	Integer.parseInt(in8);

					String accident = row[12].toString();

					String date = ""; 
					if (row[a] != null)
						date = row[a].toString();

					String vioCode = row[b].toString();

					String vioDesc ="";
					if (row[c] != null)
						vioDesc = row[c].toString();

					if(maxAddress< addressId) {
						maxAddress = addressId;
					}
					if(minAddress> addressId) {
						minAddress = addressId;
					}

					VOMovingViolations vomv = new VOMovingViolations(objId, location, addressId, streetId, xCord, yCord, ticketType, fineAmt, totalPaid, penalty1, accident, date, vioCode, vioDesc);
					//Se agrega la infraccion a la estructura(s) de datos
					queue.enqueue(vomv);
					tempInfrac++;

					if (xCord > xMax)
						xMax = xCord;

					if (yCord > yMax)
						yMax = yCord;

					if (xCord < xMin)
						xMin = xCord;

					if (yCord < yMin)
						yMin = yCord;
				}

				numInfracs += tempInfrac;
				System.out.println("En el mes de " + mths[i] + " se encontraron " + tempInfrac + " infracciones");
				parser.stopParsing();
				i++;
			}

			System.out.println("Se encontraron " + numInfracs + " infracciones en el semestre.");
			System.out.println();
			System.out.println("Minimax: " + "("+xMin+", "+yMin+"), " + "("+xMax+", "+yMax+")");
		}

		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			System.out.println();
			System.out.println("No se encontr� el archivo");
			System.out.println();
		}
	}

	/**
	 * Carga las estructuras de datos
	 */
	public void loadStructures() {
		load1B();
		load2B();
		load3B();
		load1C();
		load4C();
	}

	//-------------------------------------------------------------------------------
	//Load B
	//-------------------------------------------------------------------------------

	/**
	 * Carga la estructura de datos que será usada en el punto 1B
	 */
	public void load1B() {
		//1B
		for (int i = 0; i < list.darTamano(); i++) {
			VOMovingViolations o = list.darElemento(i);
			queue.enqueue(o);
		}
		SeparateChainingHashST<String, B1> hash1B = new SeparateChainingHashST<>();
		while(!queue.isEmpty()) {
			VOMovingViolations obj = queue.dequeue();
			String key = obj.getViolationCode();
			if (hash1B.contains(key)) {
				B1 o = hash1B.get(key);
				o.addMovingViolation(obj);
			}
			else {
				B1 o = new B1(obj);
				hash1B.put(key, o);
			}
		}
		Iterator<String> keys = hash1B.keys().iterator();
		String clave;
		b1 = new MaxPQ<>();
		while( keys.hasNext() ){	  
			clave = keys.next();
			B1 value = hash1B.get(clave);
			value.calculatePercentge();
			b1.insert(value);
		}
	}

	/**
	 * Carga la estructura de datos que será usada en el punto 2B
	 */
	public void load2B() {
		//2B
		for (int i = 0; i < list.darTamano(); i++) {

			VOMovingViolations o = list.darElemento(i);
			queue.enqueue(o);
		}
		SeparateChainingHashST<String, B2> hash2B = new SeparateChainingHashST<>();
		while(!queue.isEmpty()) {
			VOMovingViolations obj = queue.dequeue();
			String key = ""+obj.getXCord()+", "+obj.getYCord();
			if (hash2B.contains(key)) {
				B2 o = hash2B.get(key);
				o.addMovingViolation(obj);
			}
			else {
				B2 o = new B2(obj);
				hash2B.put(key, o);
			}
		}

		Iterator<String> keys = hash2B.keys().iterator();
		b2 = new RedBlackBST<>();
		String clave;

		while( keys.hasNext() ){	  
			clave = keys.next();
			B2 value = hash2B.get(clave);
			value.calculateData();
			String key = ""+value.getxCoord()+", "+value.getyCoord();
			b2.put2(key, value);
		}
	}

	/**
	 * Carga la estructura de datos que será usada en el punto 3B
	 */
	public void load3B() {
		for (int i = 0; i < list.darTamano(); i++) {

			VOMovingViolations o = list.darElemento(i);
			queue.enqueue(o);
		}
		SeparateChainingHashST<String, B3> hash3B = new SeparateChainingHashST<>();
		while(!queue.isEmpty()) {
			VOMovingViolations obj = queue.dequeue();
			String key = obj.getTicketIssueDate().substring(0, 13);
			if (hash3B.contains(key)) {
				B3 o = hash3B.get(key);
				o.addMovingViolation(obj);
			}
			else {
				B3 o = new B3(obj, key);
				hash3B.put(key, o);
			}
		}

		Iterator<String> keys = hash3B.keys().iterator();
		b3 = new RedBlackBST<>();
		String clave;

		while( keys.hasNext() ){	  
			clave = keys.next();
			B3 value = hash3B.get(clave);
			value.calculatePercentge();
			double key = value.getTotalFine();
			b3.put2(key, value);
		}
	}

	//-------------------------------------------------------------------------------
	//Load C
	//-------------------------------------------------------------------------------

	/**
	 * Carga la estructura de datos que será usada en el punto 1C
	 */
	public void load1C() {
		//1C
		Queue<VOMovingViolations> q = new Queue<>();
		for (int i = 0; i < list.darTamano(); i++) {
			VOMovingViolations o = list.darElemento(i);
			q.enqueue(o);
		}

		int tam = maxAddress+1-minAddress;
		c1 = new C1[tam];
		while(!q.isEmpty()) {
			VOMovingViolations obj = q.dequeue();
			int Address = obj.getAddress()-minAddress;

			if (c1[Address] != null) {
				C1 o = c1[Address];
				o.addMovingViolation(obj);
			}
			else {
				C1 o = new C1(obj);
				c1[Address] = o;
			}
		}
		for( C1 p : c1) {
			if(p!= null) {
				p.calculateData();
			}
		}
	}


	/**
	 * Carga la estructura de datos que será usada en el punto 4C
	 */
	private void load4C() {


		for (int i = 0; i < list.darTamano(); i++) {

			VOMovingViolations o = list.darElemento(i);
			queue.enqueue(o);
		}
		SeparateChainingHashST<String, C4> hash4C = new SeparateChainingHashST<>();
		while(!queue.isEmpty()) {
			VOMovingViolations obj = queue.dequeue();
			String key = obj.getViolationCode();
			if (hash4C.contains(key)) {
				C4 o = hash4C.get(key);
				o.addMovingViolation();
			}
			else {
				C4 o = new C4(key, obj);
				hash4C.put(key, o);
			}
		}
		Iterator<String> keys = hash4C.keys().iterator();
		String clave;
		c4 = new MaxPQ<>();
		while( keys.hasNext() ){	  
			clave = keys.next();
			C4 value = hash4C.get(clave);
			value.calculatePercentge(list.darTamano());
			c4.insert(value);
		}
	}

	//PARTE A

	/**
	 * Devuelve el ranking de las franjas horarias con m�s infracciones y su informaci�n.
	 * @param n Cantidad de franjas, n >= 1 && n<=24
	 */
	public void darRankingFranjas(int n) //1A
	{
		//Ordena segun la hora
		ComparadorPorSoloHora comparator = new ComparadorPorSoloHora();
		Sort.quickSort3(array, 0, array.length-1, comparator);
		//Agrega las franjas al MaxHeap
		Queue<VOMovingViolations> tempQueue = new Queue<>();
		MaxPQ<FranjaInfracciones> heap = new MaxPQ<FranjaInfracciones>();
		tempQueue.enqueue(array[0]);
		int i = 1;
		while (i<array.length)
		{
			if (comparator.compare(array[i-1], array[i]) == 0)
				tempQueue.enqueue(array[i]);
			else
			{
				int hour = array[i-1].getHour();
				FranjaInfracciones f = new FranjaInfracciones(tempQueue, hour, 0, 0, hour, 59, 59);
				heap.insert(f);
				tempQueue = new Queue<VOMovingViolations>();
				tempQueue.enqueue(array[i]);
			}
			i++;
		}
		FranjaInfracciones f = new FranjaInfracciones(tempQueue, array[i-1].getHour(), 0, 0, array[i-1].getHour(), 59, 59);
		heap.insert(f);
		//Devuelve el ranking
		for (int x = 0; x < n; x++)
		{
			FranjaInfracciones actual = heap.delMax();
			System.out.println();

			int hour = actual.gethIni();
			String hora = hour+"";
			if (hour < 10)
				hora = "0"+hora;

			System.out.println( "Intervalo: "+hora+":"+actual.getmIni()+"0"+":"+actual.getsIni()+"0"+" - "
					+hora+":"+actual.getmFin()+":"+actual.getsFin() );
			System.out.println("Total de Infracciones: "+actual.getNumInfracciones());
			System.out.println("Porcentaje infracciones sin accidente: "+actual.darPorcentajeSinAccidentes()+"%");
			System.out.println("Porcentaje infracciones con accidente: "+actual.darPorcentajeAccidentes()+"%");
			System.out.println("Valor total a pagar por las infracciones: "+"$"+actual.darTotalAPagar());
			System.out.println();
		}
	}

	/**
	 * Ordena las infracciones por localizaci�n geogr�fica
	 * post: Se ordenaron las infracciones por localizaci�n geogr�fica (xCord y yCord)
	 */
	public void ordenarPorLocalizacion(double x, double y) //2A
	{
		//Ordenar por localizacion
		ComparadorPorLocalizacion comparator = new ComparadorPorLocalizacion();
		Sort.quickSort3(array, 0, array.length-1, comparator);
		//Insertar en una Hash Table
		SeparateChainingHashST<LocalizacionGeografica, Queue<VOMovingViolations>> hash = new SeparateChainingHashST<>();
		Queue<VOMovingViolations> tempQueue = new Queue<VOMovingViolations>();
		tempQueue.enqueue(array[0]);
		int i = 1;
		while (i<array.length)
		{
			if (comparator.compare(array[i-1], array[i]) == 0)
				tempQueue.enqueue(array[i]);
			else
			{
				LocalizacionGeografica lg = new LocalizacionGeografica(array[i-1].getXCord(), array[i-1].getYCord());
				hash.put(lg, tempQueue);
				tempQueue = new Queue<VOMovingViolations>();
				tempQueue.enqueue(array[i]);
			}
			i++;
		}
		LocalizacionGeografica lg = new LocalizacionGeografica(array[i-1].getXCord(), array[i-1].getYCord());
		hash.put(lg, tempQueue);
		//Devolver localizacion dada
		LocalizacionGeografica key = new LocalizacionGeografica(x, y);
		Queue<VOMovingViolations> queue = hash.get(key);
		if (queue == null)
		{
			System.out.println();
			System.out.println("No se encontraron infracciones con la localizaci�n dada");
		}
		else
		{
			System.out.println();
			System.out.println("N�mero de infracciones: " + queue.size());
			int accidentes = 0;
			int noAccidentes = 0;
			double totalToPay = 0.0;
			for (VOMovingViolations current: queue)
			{
				if (current.getAccidentIndicator().equalsIgnoreCase("Yes"))
					accidentes++;
				else
					noAccidentes++;
				totalToPay += current.getTotalPaid();
			}
			double porAccidentes = (double) accidentes*100/queue.size();
			double porNoAccidentes = (double) noAccidentes*100/queue.size();
			VOMovingViolations element = queue.dequeue();
			System.out.println("Porcentaje de infracciones sin accidente: " + porNoAccidentes + "%");
			System.out.println("Porcentaje de infracciones con accidente: " + porAccidentes + "%");
			System.out.println("Valor total a pagar por las infracciones: $" + totalToPay);
			System.out.println("Location: " + element.getLocation());
			System.out.println("AddressID: " + element.getAddress());
			System.out.println("StreetSegID: " + element.getStreetSegId());
		}
		System.out.println();
	}

	/**
	 * Devuelve todas las infracciones en un rango de fechas especifico
	 * @param di Fecha inicial
	 * @param df Fecha final
	 */
	public void darInfraccionesPorFechas(Fecha di, Fecha df) //3A
	{
		ComparadorPorFecha comparator = new ComparadorPorFecha();
		Sort.quickSort3(array, 0, array.length-1, comparator);
		//Insertar en el arbol
		RedBlackBST<Fecha, Queue<VOMovingViolations>> bst = new RedBlackBST<>();
		Queue<VOMovingViolations> tempQueue = new Queue<VOMovingViolations>();
		tempQueue.enqueue(array[0]);
		int i = 1;
		while (i<array.length)
		{
			if (comparator.compare(array[i-1], array[i]) == 0)
				tempQueue.enqueue(array[i]);
			else
			{
				Fecha date = new Fecha(array[i-1].getDate());
				bst.put(date, tempQueue);
				tempQueue = new Queue<VOMovingViolations>();
				tempQueue.enqueue(array[i]);
			}
			i++;
		}
		Fecha d = new Fecha(array[i-1].getDate());
		bst.put(d, tempQueue);
		//Devolver las infracciones por el rango
		Iterator<Queue<VOMovingViolations>> it = bst.valuesInRange(di, df);
		while (it.hasNext())
		{
			Queue<VOMovingViolations> queue = it.next();
			VOMovingViolations element = queue.dequeue();
			Fecha date = element.getDate();
			int mes = date.getMes();
			String mth = mes+"";
			if (mes < 10)
				mth = "0"+mth;
			int dia = date.getDia();
			String day = dia+"";
			if (dia < 10)
				day = "0"+day;
			System.out.println();
			System.out.println("Fecha: "+date.getAno()+"/"+mth+"/"+day);
			queue.enqueue(element);
			int totalVomv = queue.size();
			int accidentes = 0;
			int noAccidentes = 0;
			double totalToPay = 0.0;
			for (VOMovingViolations current: queue)
			{
				if (current.getAccidentIndicator().equalsIgnoreCase("Yes"))
					accidentes++;
				else
					noAccidentes++;
				totalToPay += current.getTotalPaid();
			}
			double porAccidentes, porNoAccidentes;
			porAccidentes = (double) accidentes*100/totalVomv;
			porNoAccidentes = (double) noAccidentes*100/totalVomv;
			System.out.println("Total de infracciones: " + totalVomv);
			System.out.println("Porcentaje de infracciones sin accidente: "+porNoAccidentes+"%");
			System.out.println("Porcentaje de infracciones con accidente: "+porAccidentes+"%");
			System.out.println("Valor total a pagar por las infracciones: "+"$"+totalToPay);
		}
		System.out.println();
	}

	//PARTE B

	/**
	 * Devuelve el top n en el ranking de tipos de infraccion con mas infracciones
	 * @param n Top elementos
	 * @return
	 */
	public	void darRankingVioCode(int n) //1B
	{
		req1B = new Queue<>();
		for (int i = 0; i < n; i++) {
			req1B.enqueue(b1.delMax());
		}
	}

	public void ordenarPorLocalizacionB(double xcoord,  double ycoord) //Requerimiento repetido 2B
	{
		String key = xcoord+", "+ycoord;
		req2B = b2.get(key);
	}

	/**
	 * Busca las franjas en donde se ha acumulado un rango de valores
	 * @param valIni Valor inicial
	 * @param valFin Valor Final
	 * valIni < valFin
	 */
	public void darAcumuladoInfracciones(double valIni, double valFin) //3B
	{
		reqB3 = new Queue<>();
		Iterator<B3>iterator = b3.valuesInRange(valIni, valFin);
		while(iterator.hasNext()) {
			reqB3.enqueue( iterator.next());	
		}
	}

	//PARTE C

	/**
	 * Devuelve la informacion 
	 * @param address
	 */
	public void darInformacionPorAddressID(int address) //1C
	{
		req1C = c1[address-minAddress];
	}

	/**
	 * Devuelve las infracciones por un rango de horas dado
	 * @param h1 Hora inicial
	 * @param h2 Hora final
	 * d1 > d2
	 */
	public void darInfraccionesRangoHoras(Hora h1, Hora h2) //2C
	{
		t1 = System.nanoTime();
		Iterator<Queue<VOMovingViolations>> it = tree.valuesInRange(h1, h2);
		if (!it.hasNext())
		{
			System.out.println("No se encontraron infracciones en el rango de horas dado");
			return;
		}
		SeparateChainingHashST<String, Integer> hash = new SeparateChainingHashST<>();
		ArrayList<String> keys = new ArrayList<>();
		int total = 0;
		int accidentes = 0;
		int noAccidentes = 0;
		double totalToPay = 0.0;
		while (it.hasNext())
		{
			Queue<VOMovingViolations> current = it.next();
			total += current.size();
			for (VOMovingViolations vomv: current)
			{
				//Req 1
				if (vomv.getAccidentIndicator().contains("Yes"))
					accidentes++;
				else
					noAccidentes++;
				totalToPay += vomv.getTotalPaid();
				//Req 2
				String key = vomv.getViolationCode();
				if (keys.contains(key))
				{
					int preValue = hash.get(key);
					hash.put( key, (preValue+1) );
				}
				else
				{
					keys.add(key);
					hash.put(key, 1);
				}
			}
		}
		double porAccidentes = (double) accidentes*100/total;
		double porNoAccidentes = (double) noAccidentes*100/total;
		t2 = System.nanoTime();
		System.out.println();
		System.out.println("Total de infracciones: " + total);
		System.out.println("Infracciones sin accidente: " + porNoAccidentes + "%");
		System.out.println("Infracciones con accidente: " + porAccidentes + "%");
		System.out.println("Valor total a pagar por las infracicones: $" + totalToPay);
		System.out.println();

		for (String currentKey: keys)
		{
			System.out.println("C�digo de infracci�n: " + currentKey);
			System.out.println("N�mero de infracciones: " + hash.get(currentKey));
			System.out.println();
		}
	}

	/**
	 * Devuelve el top n del ranking de las localizaciones geogr�ficas con m�s infracciones (xCord, yCord)
	 * @param n Top n localizaciones
	 */
	public void darRankingLocalizaciones(int n) //3C
	{
		System.out.println();
		t1 = System.nanoTime();
		int i = 0;
		while ( i < n )
		{
			InformacionLocalizacion temp = null;
			try
			{
				temp = heap.delMax();
			}
			catch (NoSuchElementException e)
			{
				t2 = System.nanoTime();
				System.out.println("No hay m�s localizaciones (El top es mayor que el n�mero de localizaciones)");
				return;
			}
			Queue<VOMovingViolations> tempQueue = temp.getQueue();
			int total = tempQueue.size();
			System.out.println("Top: "+(i+1));
			System.out.println("Localizaci�n: " + temp.getXCord() +" - "+ temp.getYCord());
			System.out.println("Total de infracciones: " + total);
			VOMovingViolations element = tempQueue.dequeue();
			tempQueue.enqueue(element);
			int accidents = 0;
			int noAccidents = 0;
			for (VOMovingViolations current: tempQueue)
			{
				if (current.getAccidentIndicator().equalsIgnoreCase("Yes"))
					accidents++;
				else
					noAccidents++;
			}
			double porAccidentes = (double) accidents*100/total;
			double porNoAccidentes = (double) noAccidents*100/total;
			System.out.println("Porcentaje de infracciones sin accidente: " + porNoAccidentes +"%");
			System.out.println("Porcentaje de infracciones con accidente: " + porAccidentes + "%");
			System.out.println("Location: "+element.getLocation());
			System.out.println("AddressID: "+element.getAddress());
			System.out.println("StreetSegID: "+element.getStreetSegId());
			System.out.println();

			i++;
		}
		t2 = System.nanoTime();
	}
}
