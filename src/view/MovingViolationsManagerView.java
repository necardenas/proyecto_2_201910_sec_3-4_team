package view;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.MaxPQ;
import model.vo.B1;
import model.vo.B2;
import model.vo.B3;
import model.vo.C1;
import model.vo.C4;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{
		
	}

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos------------");
		System.out.println("---------------------Proyecto 2----------------------");
		//EXIT
		System.out.println("0. Salir");
		//EXIT
		//LOAD
		System.out.println("1. Cargar datos");
		//LOAD
		System.out.println("---------------------PARTE A-------------------------");
		System.out.println("2. Dar el ranking para n franjas con m�s infracciones (Hash Table)");
		System.out.println("3. Ordenar infracciones por localizaci�n geogr�fica");
		System.out.println("4. Buscar infracciones por rango de fechas");
		System.out.println("---------------------PARTE B-------------------------");
		System.out.println("5. Obtener el ranking de n tipos de infraccion con m�s infracciones");
		System.out.println("6. Dar el ranking para n franjas con m�s infracciones (�rbol de B�squeda Balanceado)");
		System.out.println("7. Buscar franjas con valor acomulado");
		System.out.println("---------------------PARTE C-------------------------");
		System.out.println("8. Dar informaci�n de una localizaci�n");
		System.out.println("9. Obtener infracciones en un rango de horas");
		System.out.println("10. Dar ranking de n localizaciones geogr�ficas con m�s infracciones");
		System.out.println("11. Graficar c�digos con m�s infracciones");
		System.out.println("--- Digite el numero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1): ---");
	}

	public void printMovingViolations(IQueue<VOMovingViolations> violations) 
	{
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (VOMovingViolations violation : violations) 
		{
			System.out.println(violation.getObjectId() + " " 
					+ violation.getTicketIssueDate() + " " 
					+ violation.getLocation()+ " " 
					+ violation.getViolationDescription());
		}
	}

	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}
	
	public void printReq1B(IQueue<B1> resultados) {
		for(B1 infraVioCode: resultados) {
			System.out.println("Codigo: "+ infraVioCode.getViolationCode());
			System.out.println("El valor total de infracciones fue de: "+ infraVioCode.getTotalMoviongViolations());
			System.out.println("El porcentaje de infracciones con accidentes fue de: "+infraVioCode.getAccidentRate());
			System.out.println("El porcentaje de infracciones sin accidentes fue de: "+infraVioCode.getNoAccidentRate());
			System.out.println("El valor acumulado de las infracciones: "+infraVioCode.getTotalFine());
		}
	}

	public void printReq2B(B2 obj) {
		System.out.println("Total infracciones: "+ obj.getTotalAccidents());
		System.out.println("Porcentaje de accidentes: "+obj.getAccidentRate());
		System.out.println("porcentaje sin accidente: "+ obj.getNoAccidentRate());
		System.out.println("Multa total : "+ obj.getTotalFine());
		System.out.println("AddressID: " + obj.getAddressId());
		System.out.println("StreetSegId: "+ obj.getStreetSegID());
		System.out.println();
	}


	public void printReq3B(IQueue<B3> resultados) {
		for(B3 obj: resultados) {
			System.out.println("Franja horaria: "+obj.getDate());
			System.out.println("Multa acumulada: "+obj.getTotalFine());
			System.out.println("Total infracciones: "+obj.getTotalMovingViolations());
			System.out.println("procentaja accidentes: "+obj.getAccidentRate());
			System.out.println("porentaje sin accidentes:"+ obj.getNoAccidentRate());
			System.out.println();
		}
	}

	public void printReq1C(C1 obj) {
		System.out.println("Numero infracciones "+ obj.getTotalMovingViolations());
		System.out.println("Infracciones con acidente "+obj.getAccidentRate());
		System.out.println("Infracciones sin accidente"+obj.getNoAccidentRate());
		System.out.println("Valor Multas "+ obj.getTotalFine());
		System.out.println("STREETSEGID: "+ obj.getStreetSegID());
		/* Detalle de las infracciones (Se requiere SOLO en caso de validacion)*/
		/*		
		for(VOMovingViolations v: obj.getListaInfracciones()) {
			System.out.println(v.toString());
		}
		 */
	}

	public void printReq4C(MaxPQ<C4> c4) {
		while (!c4.isEmpty()) {
			C4 obj = c4.delMax();
			String code = obj.getViolationCode();
			int percentage = obj.getPercentege();
			String x = "";
			for (int i = 0; i < percentage; i++) {
				x = x+"*";
			}
			System.out.println(code+" | "+x);

		}
		System.out.println();
		System.out.println("Cada * representa un 1% ");	
	}
}
