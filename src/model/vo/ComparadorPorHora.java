package model.vo;

import java.util.Comparator;

public class ComparadorPorHora implements Comparator<VOMovingViolations>
{

	@Override
	public int compare(VOMovingViolations v1, VOMovingViolations v2) {
		return v1.getHourObject().compareTo(v2.getHourObject());
	}

}
