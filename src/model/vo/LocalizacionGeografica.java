package model.vo;

public class LocalizacionGeografica implements Comparable<LocalizacionGeografica>
{
	private double x;

	private double y;

	public LocalizacionGeografica(double x, double y)
	{
		this.x=x;this.y=y;
	}

	public int compareTo(LocalizacionGeografica that)
	{
		if (x > that.x)
			return 1;
		else if (x < that.x)
			return -1;
		else
		{
			if (y > that.y)
				return 1;
			else if (y < that.y)
				return -1;
			else
				return 0;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalizacionGeografica other = (LocalizacionGeografica) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

}
