package model.vo;

import java.util.Comparator;

public class ComparadorPorSoloHora implements Comparator<VOMovingViolations>
{
	@Override
	public int compare(VOMovingViolations v1, VOMovingViolations v2) {
		if (v1.getHour() > v2.getHour())
			return 1;
		else if (v1.getHour() < v2.getHour())
			return -1;
		else
			return 0;
	}
}
