package model.vo;

public class Fecha implements Comparable<Fecha>
{
	private int ano;
	
	private int mes;
	
	private int dia;

	public Fecha(Fecha f)
	{
		ano = f.ano;
		mes = f.mes;
		dia = f.dia;
	}
	
	public Fecha(int ano, int mes, int dia) {
		this.ano = ano;
		this.mes = mes;
		this.dia = dia;
	}

	/**
	 * @return the a�o
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * @return the mes
	 */
	public int getMes() {
		return mes;
	}

	/**
	 * @return the dia
	 */
	public int getDia() {
		return dia;
	}

	@Override
	public int compareTo(Fecha that) {
		if (ano > that.ano)
			return 1;
		else if (ano < that.ano)
			return -1;
		else
		{
			if (mes > that.mes)
				return 1;
			else if (mes < that.mes)
				return -1;
			else
			{
				if (dia > that.dia)
					return 1;
				else if (dia < that.dia)
					return -1;
				else
					return 0;
			}
		}
	}
	
}
