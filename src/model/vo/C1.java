package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class C1 {

	Queue<VOMovingViolations> accidentQueue;

	Queue<VOMovingViolations> noAccidentQueue;

	private int totalMovingViolations;
	
	private double accidentRate;
	
	private double noAccidentRate;

	private double totalFine;

	private double streetSegID;

	public C1(VOMovingViolations obj) {
		super();
		
		this.streetSegID = obj.getStreetSegId();
		this.accidentQueue = new Queue<>();
		this.noAccidentQueue = new Queue<>();
		if(obj.getAccidentIndicator().equals("Yes")){
			
			accidentQueue.enqueue(obj);
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
		
		}
		else{
			noAccidentQueue.enqueue(obj);	
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			
		}
	
		//calculateData();
	}

	public void addMovingViolation(VOMovingViolations obj){

		if(obj.getAccidentIndicator().equals("Yes")){		
			accidentQueue.enqueue(obj);
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			//calculateData();

			
		}
		else{
			noAccidentQueue.enqueue(obj);	
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			//calculateData();
	
		}

	}
	
	public void calculateData(){
		accidentRate = (accidentQueue.size()*100)/totalMovingViolations;
		noAccidentRate = 100 -accidentRate;
		while(!noAccidentQueue.isEmpty()) {
			VOMovingViolations obj = noAccidentQueue.dequeue();
		}
	}

	public int getTotalMovingViolations() {
		return totalMovingViolations;
	}

	public double getAccidentRate() {
		return accidentRate;
	}

	public double getNoAccidentRate() {
		return noAccidentRate;
	}

	public double getTotalFine() {
		return totalFine;
	}

	public double getStreetSegID() {
		return streetSegID;
	}
	
	public IQueue<VOMovingViolations> getListaInfracciones(){
		IQueue<VOMovingViolations> x =  new Queue<>();
		while(!accidentQueue.isEmpty()){
			x.enqueue(accidentQueue.dequeue());
			
		}
		while(!noAccidentQueue.isEmpty()) {
			x.enqueue(noAccidentQueue.dequeue());
		}
		return x;
	}
	
	

}