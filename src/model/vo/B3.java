package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class B3 implements Comparable<B3>{

	private String date;
	
	private int totalMovingViolations;
	
	private int totalAccidents;
	
	private int accidentRate;
	
	private int noAccidentRate;
	
	private double totalFine;
	
	private IQueue<VOMovingViolations> queue;
	
	public B3(VOMovingViolations obj, String date ) {
		this.totalFine = 0;
		this.date = date;
		this.queue = new Queue<>();
		if(obj.getAccidentIndicator().equals("Yes")){
			totalMovingViolations = 1;
			totalFine = totalFine + obj.getFineAmt();
			totalAccidents=1;
			queue.enqueue(obj);
		}
		else{
			totalMovingViolations =1 ;
			totalFine = totalFine + obj.getFineAmt();
			queue.enqueue(obj);
		}
	}


	public void addMovingViolation(VOMovingViolations obj){
		if(obj.getAccidentIndicator().equals("Yes")){
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			totalAccidents++;
			queue.enqueue(obj);
		}
		else{
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			queue.enqueue(obj);
		}
	}

	public void calculatePercentge(){
		accidentRate = (totalAccidents*100)/totalMovingViolations;
		noAccidentRate = 100 -accidentRate;
	}
	
	public String getDate() {
		return date;
	}


	public int getTotalMovingViolations() {
		return totalMovingViolations;
	}


	public int getAccidentRate() {
		return accidentRate;
	}


	public int getNoAccidentRate() {
		return noAccidentRate;
	}


	public double getTotalFine() {
		return totalFine;
	}
	
	public IQueue<VOMovingViolations> getListaInfracciones(){
		return queue;
	}


	@Override
	public int compareTo(B3 o) {
		double x = totalFine - o.getTotalFine();
		if(x>0) {
			return 1;
		}
		if(x<0) {
			return -1;
		}
		else return 0; 
	}


}
