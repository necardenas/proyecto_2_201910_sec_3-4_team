package model.vo;

public class Hora implements Comparable<Hora>
{
	private int hora;
	
	private int min;
	
	private int seg;

	public Hora(Hora h)
	{
		hora = h.getHora();
		min = h.getMinuto();
		seg = h.getSegundo();
	}
	
	public Hora(int hora, int minuto, int segundo) {
		this.hora = hora;
		this.min = minuto;
		this.seg = segundo;
	}

	/**
	 * @return the hora
	 */
	public int getHora() {
		return hora;
	}

	/**
	 * @return the minuto
	 */
	public int getMinuto() {
		return min;
	}

	/**
	 * @return the segundo
	 */
	public int getSegundo() {
		return seg;
	}

	@Override
	public int compareTo(Hora that) {
		if (hora > that.hora)
			return 1;
		else if (hora < that.hora)
			return -1;
		else
		{
			if (min > that.min)
				return 1;
			else if (min < that.min)
				return -1;
			else
			{
				if (seg > that.seg)
					return 1;
				else if (seg < that.seg)
					return -1;
				else
					return 0;
			}
		}
	}

}
