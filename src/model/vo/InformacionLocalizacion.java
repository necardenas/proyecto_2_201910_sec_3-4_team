package model.vo;

import model.data_structures.Queue;

public class InformacionLocalizacion implements Comparable<InformacionLocalizacion>
{
	private double x;

	private double y;
	
	private int numInfracciones;
	
	private Queue<VOMovingViolations> queue;

	
	
	public InformacionLocalizacion(double x, double y, Queue<VOMovingViolations> queue)
	{
		this.x=x;
		this.y=y;
		numInfracciones = queue.size();
		this.queue = queue;
	}

	public Queue<VOMovingViolations> getQueue()
	{
		return queue;
	}
	
	public double getXCord()
	{
		return x;
	}
	
	public double getYCord()
	{
		return y;
	}
	
	//CompareTo
	
	public int compareTo(InformacionLocalizacion that)
	{
		if (numInfracciones > that.numInfracciones)
			return 1;
		else if (numInfracciones < that.numInfracciones)
			return -1;
		else
			return 0;
	}
}
