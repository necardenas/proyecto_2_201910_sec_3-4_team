package model.vo;

import java.util.Comparator;

public class ComparadorPorFecha implements Comparator<VOMovingViolations>
{

	@Override
	public int compare(VOMovingViolations v1, VOMovingViolations v2) {
		return v1.getDate().compareTo(v2.getDate());
	}
	
}
