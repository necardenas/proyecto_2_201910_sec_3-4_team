package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class B2 implements Comparable<B2> {


	
	Queue<VOMovingViolations> accidentQueue;

	Queue<VOMovingViolations> noAccidentQueue;

	private double xCoord;

	private double yCoord;

	private double accidentRate;
	
	private double noAccidentRate;
	
	private int totalMovingViolations;

	private double totalFine;

	private String location;

	private int addressId;

	private double streetSegID;

	public B2(VOMovingViolations obj) {
		super();
		this.accidentQueue = new Queue<>();
		this.noAccidentQueue = new Queue<>();
		if(obj.getAccidentIndicator().equals("Yes")){
			
			accidentQueue.enqueue(obj);
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
		}
		else{
			
			noAccidentQueue.enqueue(obj);
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
		}

		this.xCoord = obj.getXCord();
		this.yCoord = obj.getYCord();
		this.location = obj.getLocation();
		this.addressId = obj.getAddress();
		this.streetSegID = obj.getStreetSegId();
	}

	public void addMovingViolation(VOMovingViolations obj){

		if(obj.getAccidentIndicator().equals("Yes")){
			
			accidentQueue.enqueue(obj);
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
		}
		else{
			
			noAccidentQueue.enqueue(obj);	
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
		}

	}



	public void calculateData(){
		accidentRate = (accidentQueue.size()*100)/totalMovingViolations;
		noAccidentRate = 100 -accidentRate;
	}

	

	public double getxCoord() {
		return xCoord;
	}

	public double getyCoord() {
		return yCoord;
	}

	public double getAccidentRate() {
		return accidentRate;
	}

	public double getNoAccidentRate() {
		return noAccidentRate;
	}

	public int getTotalAccidents() {
		return totalMovingViolations;
	}

	public double getTotalFine() {
		return totalFine;
	}

	public String getLocation() {
		return location;
	}

	public int getAddressId() {
		return addressId;
	}

	public double getStreetSegID() {
		return streetSegID;
	}

	@Override
	public int compareTo(B2 o) {

		int value = 0;
		double p = this.xCoord - o.xCoord;
		if(p == 0){
			p = this.yCoord - o.yCoord;
		}	
		if(p >0) {
			value = 1;
		}
		else if(p <0) {
			value = -1;
		}
		return value;
	}

	public IQueue<VOMovingViolations> getListaInfracciones(){
		IQueue<VOMovingViolations> x =  new Queue<>();
		while(!accidentQueue.isEmpty()){
			x.enqueue(accidentQueue.dequeue());
			
		}
		while(!noAccidentQueue.isEmpty()) {
			x.enqueue(noAccidentQueue.dequeue());
		}
		return x;
	}



}
