package model.vo;
import java.util.Comparator;
import model.vo.*;

public class ComparadorPorLocalizacion implements Comparator<VOMovingViolations>
{
	public int compare(VOMovingViolations v1, VOMovingViolations v2) {
		if (v1.getXCord() > v2.getXCord())
			return 1;
		else if (v1.getXCord() < v2.getXCord())
			return -1;
		else
		{
			if (v1.getYCord() > v2.getYCord())
				return 1;
			else if (v1.getYCord() < v2.getYCord())
				return -1;
			else
				return 0;
		}
	}
}
