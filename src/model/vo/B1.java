package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class B1 implements Comparable<B1>{

	private String violationCode;

	private int totalMovingViolations;
	
	private int totalAccidents;

	private double accidentRate;

	private double noAccidentRate;
	
	private double totalFine;

	private IQueue<VOMovingViolations> queue;
	
	public B1( VOMovingViolations obj  ){


		this.totalFine = 0;
		this.violationCode = obj.getViolationCode();
		this.queue = new Queue<>();
		if(obj.getAccidentIndicator().equals("Yes")){
			totalMovingViolations = 1;
			totalFine = totalFine + obj.getFineAmt();
			totalAccidents=1;
			queue.enqueue(obj);
		}
		else{
			totalMovingViolations =1 ;
			totalFine = totalFine + obj.getFineAmt();
			queue.enqueue(obj);
		}
	}


	public void addMovingViolation(VOMovingViolations obj){
		if(obj.getAccidentIndicator().equals("Yes")){
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			totalAccidents++;
			queue.enqueue(obj);
		}
		else{
			totalMovingViolations ++;
			totalFine = totalFine + obj.getFineAmt();
			queue.enqueue(obj);
		}
		
	}

	public String getViolationCode() {
		return violationCode;
	}

	public double getTotalFine() {
		return totalFine;
	}

	public double getAccidentRate() {
		return accidentRate;
	}


	public double getNoAccidentRate() {
		return noAccidentRate;
	}

	public void calculatePercentge(){
		accidentRate = (totalAccidents*100)/totalMovingViolations;
		noAccidentRate = 100 -accidentRate;
	}

	public int getTotalMoviongViolations() {
		return totalMovingViolations;
	}

	public IQueue<VOMovingViolations> getListaInfracciones(){
		return queue;
	}
	@Override
	public int compareTo(B1 o) {
		return totalMovingViolations - o.getTotalMoviongViolations();
	}
}


