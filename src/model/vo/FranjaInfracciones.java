package model.vo;

import java.time.LocalDateTime;

import model.data_structures.Queue;

public class FranjaInfracciones implements Comparable<FranjaInfracciones>
{
	private int numInfracciones;

	private int hIni, mIni, sIni;

	private int hFin, mFin, sFin;

	private Queue<VOMovingViolations> infracciones;

	public FranjaInfracciones(Queue<VOMovingViolations> queue, int hIni, int mIni, int sIni, int hFin, int mFin, int sFin)
	{
		infracciones = queue; numInfracciones = queue.size();
		this.hIni = hIni; this.mIni = mIni; this.sIni = sIni;
		this.hFin = hFin; this.mFin = mFin; this.sFin = sFin;
	}
	
	/**
	 * @return the hIni
	 */
	public int gethIni() {
		return hIni;
	}


	/**
	 * @return the mIni
	 */
	public int getmIni() {
		return mIni;
	}


	/**
	 * @return the sIni
	 */
	public int getsIni() {
		return sIni;
	}


	/**
	 * @return the hFin
	 */
	public int gethFin() {
		return hFin;
	}


	/**
	 * @return the mFin
	 */
	public int getmFin() {
		return mFin;
	}


	/**
	 * @return the sFin
	 */
	public int getsFin() {
		return sFin;
	}

	/**
	 * @return the infracciones
	 */
	public Queue<VOMovingViolations> getInfracciones() {
		return infracciones;
	}

	/**
	 * @return the numInfracciones
	 */
	public int getNumInfracciones() {
		return numInfracciones;
	}

	public double darPorcentajeAccidentes()
	{
		int accidentes = 0;
		for (VOMovingViolations current: infracciones)
		{
			if (current.getAccidentIndicator().equalsIgnoreCase("Yes"))
				accidentes++;
		}
		return (double) accidentes*100/numInfracciones;
	}

	public double darPorcentajeSinAccidentes()
	{
		int noAccidentes = 0;
		for (VOMovingViolations current: infracciones)
		{
			if (current.getAccidentIndicator().equalsIgnoreCase("No"))
				noAccidentes++;
		}
		return (double) noAccidentes*100/numInfracciones;
	}

	public double darTotalAPagar()
	{
		double total = 0;
		for (VOMovingViolations current: infracciones)
		{
			total += current.getTotalPaid();
		}
		return total;
	}

	@Override
	public int compareTo(FranjaInfracciones that) 
	{
		if (this.numInfracciones > that.numInfracciones)
			return 1;
		else if (this.numInfracciones < that.numInfracciones)
			return -1;
		else
			return 0;		
	}
}
