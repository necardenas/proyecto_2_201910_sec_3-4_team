package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class C4 implements Comparable<C4> {

	private String violationCode;
	
	private int totalMoviongViolations;
	
	private int percentage;
	
	private IQueue<VOMovingViolations> queue;
	
	public C4( String violationCode, VOMovingViolations o  ){
		queue = new Queue<>();
		this.totalMoviongViolations = 1;
		this.violationCode = violationCode;
		queue.enqueue(o);
	}

	
	public void addMovingViolation(){
		totalMoviongViolations++;
	}
	
	public String getViolationCode() {
		return violationCode;
	}

	public int getPercentege(){
		return percentage;
	}
	
	public void calculatePercentge(int total){
		percentage = Math.round(((totalMoviongViolations*100)/total));
	}
	
	public int getTotalMoviongViolations() {
		return totalMoviongViolations;
	}


	@Override
	public int compareTo(C4 o) {
		return totalMoviongViolations - o.getTotalMoviongViolations();
	}
	
	public IQueue<VOMovingViolations> getListaInfracciones(){
		return queue;
	}
}
